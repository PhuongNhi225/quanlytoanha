create database buildingmanager;

use buildingmanager;

create table user(
id int primary key auto_increment,
username varchar(16),
password text,
name nvarchar(100),
gender boolean,
phone varchar(11),
email varchar(50),
address text,
dob date
);

create table role(
id int primary key auto_increment,
name nvarchar(50)
);
create table userrole(
id_user int,
id_role int,
primary key(id_user, id_role),
constraint fk_user foreign key (id_user) references user(id),
constraint fk_role foreign key (id_role) references role(id)

);

create table company(
id int primary key auto_increment,
name nvarchar(225),
address text
);

create table companyuser(
id_company int,
id_user int,
primary key(id_company, id_user),
constraint fk_company foreign key (id_company) references company(id),
constraint fk_user_companyuser foreign key (id_user) references user(id)
);

create table building(
id int primary key auto_increment,
name nvarchar(50),
id_company int,
address text,
constraint fk_building_company foreign key (id_company) references company(id)
);

create table area(
id int primary key auto_increment,
name nvarchar(50),
id_building int,
constraint fk_area_building foreign key (id_building) references building(id)
);

create table floor(
id int primary key auto_increment,
name nvarchar(50),
id_area int,
constraint fk_floor_area foreign key (id_area) references area(id)
);

create table imagetype(
id int primary key auto_increment,
name nvarchar(50)
);
create table image(
id int primary key auto_increment,
id_building int,
url text,
id_type int,
constraint fk_image_building foreign key (id_building) references building(id),
constraint fk_image_imagetype foreign key (id_type) references imagetype(id)
);

create table room (
id int primary key auto_increment,
id_floor int,
unit_price decimal,
constraint fk_floor_room foreign key (id_floor) references floor(id)
);

create table pmanufacture (
id int primary key auto_increment,
name nvarchar(50) not null
);

create table porigin (
id int primary key auto_increment,
name nvarchar(50) not null
);

create table pcategory (
id int primary key auto_increment,
name nvarchar(50) not null
);

create table property (
id int primary key auto_increment,
id_category int not null,
id_origin int not null,
id_mnft int not null,
id_room int not null,
constraint fk_idroom_property foreign key (id_room) references room(id),
constraint fk_idmnft_property foreign key (id_mnft) references pmanufacture(id),
constraint fk_idorigin_property foreign key (id_origin) references porigin(id),
constraint fk_idcategory_property foreign key (id_category) references pcategory(id)
);

create table propertyusage (
id int primary key auto_increment,
id_property int not null,
time_on datetime not null,
time_off datetime not null,
constraint fk_idproperty_propertyusage foreign key (id_property) references property(id)
);

create table orderstatus (
id int primary key auto_increment,
`name` nvarchar(20) not null
);

create table `order`(
id int primary key auto_increment,
id_user int not null,
id_status int not null,
total_cost decimal not null,
create_time datetime not null,
constraint fk_iduser_order foreign key (id_user) references user(id),
constraint fk_idstatus_order foreign key (id_status) references orderstatus(id)
);

create table orderdetail (
id_order int not null,
id_room int not null,
start_time datetime not null,
end_time datetime not null,
return_time datetime not null,
unit_price decimal not null,
primary key (id_order, id_room),
constraint fk_idroom_orderdetail foreign key (id_room) references room(id),
constraint fk_idorder_orderdetail foreign key (id_order) references `order`(id)
);

create table fbcategory(
id int primary key auto_increment,
name nvarchar(20)
);

create table feedback(
id int primary key auto_increment,
id_property int,
id_category int,
id_user int,
detail text,
status nvarchar(20),
constraint fk_property_feedback foreign key(id_property) references property(id),
constraint fk_category_feedback foreign key(id_category) references fbcategory(id),
constraint fk_user_feedback foreign key(id_user) references user(id)
);

create table trequesttype(
id int primary key auto_increment,
name nvarchar(20)
);

create table technicalrequest(
id int primary key auto_increment,
id_requester int,
id_receiver int,
id_requesttype int,
creation_time datetime,
constraint fk_requester_technicalrequest foreign key(id_requester) references user(id),
constraint fk_receiver_technicalrequest foreign key(id_receiver) references user(id),
constraint fk_requesttype_technicalrequest foreign key(id_requesttype) references trequesttype(id)
);

create table trequestdetail(
id_request int,
id_property int,
id_feedback int,
details text,
status nvarchar(50),
completion_time datetime,
report_detail text,
primary key(id_request, id_property, id_feedback),
constraint fk_request_trequestdetail foreign key(id_request) references technicalrequest(id),
constraint fk_property_trequestdetail foreign key(id_property) references property(id),
constraint fk_feedback_trequestdetail foreign key(id_feedback) references feedback(id)

);